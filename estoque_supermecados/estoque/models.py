from django.db import models
from decimal import Decimal

class Produto(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)

    def __str__(self):
        return self.nome

class Compra(models.Model):
    id = models.AutoField(primary_key=True)
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    quantidade = models.IntegerField(default=0)
    preco_compra = models.DecimalField(max_digits=8, decimal_places=2)

    def precoMedio(self):
        valor = ("%.2f" % (self.preco_compra / self.quantidade))
        return valor

    def __str__(self):
        return "{}: {}qtd - R${}".format(self.produto, self.quantidade, self.preco_compra)