from django.conf.urls import url

from .import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^compras/$', views.listaCompras, name='compras'),
    url(r'^compras/novo/$', views.cadastraCompra, name='cadastraCompra'),
    url(r'^compras/cadastra/$', views.salvaCompra, name='salvaCompra'),
    url(r'^compras/excluir/(?P<id>[0-9]+)$', views.excluiCompra, name='excluiCompra'),
    url(r'^produtos/novo/$', views.cadastraProduto, name='cadastraProduto'),
    url(r'^produtos/cadastra/$', views.salvaProduto, name='salvaProduto'),
    url(r'^produtos/atualizar/(?P<id>[0-9]+)$', views.atualizaProduto, name='atualizarProduto'),
    url(r'^produtos/excluir/(?P<id>[0-9]+)$', views.excluiProduto, name='excluiProduto'),
    url(r'^produtos/$', views.listaProdutos, name='produtos'),
    url(r'^produtos/(?P<id>[0-9]+)$', views.editaProduto, name='editaProduto'),
]