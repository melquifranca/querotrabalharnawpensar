from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect
from .models import Produto, Compra

def index(request):
    produtos = Produto.objects.all()
    compras = Compra.objects.all()
    template = loader.get_template('painel.html')
    return HttpResponse(template.render({
        'qtdProdutos': len(produtos),
        'qtdCompras': len(compras),
    }))

def listaProdutos(request):
    produtos = Produto.objects.all()
    template = loader.get_template('produtos/index.html')
    return HttpResponse(template.render({
        'produtos': produtos,
    }))

def editaProduto(request, id):
    produto = Produto.objects.get(pk=id)
    template = loader.get_template('produtos/editar.html')
    return HttpResponse(template.render({
        'produto': produto
    }, request))


def cadastraProduto(request):
    template = loader.get_template('produtos/cadastra.html')
    return HttpResponse(template.render({
    }, request))

def salvaProduto(request):
    nome = request.POST.get('nome')

    if len(nome) < 3:
        return HttpResponseRedirect("/estoque/produtos/novo")    
    produto = Produto(nome=nome)
    produto.save()
    return HttpResponseRedirect("/estoque/produtos")

def atualizaProduto(request, id):
    produto = Produto.objects.get(pk=id)
    nome = request.POST.get('nome')
    produto.nome = nome
    produto.save()
    return HttpResponseRedirect("/estoque/produtos")

def excluiProduto(request, id):
    produto = Produto.objects.get(pk=id)
    produto.delete()
    return HttpResponseRedirect("/estoque/produtos")

def listaCompras(request):
    compras = Compra.objects.all()
    template = loader.get_template('compras/index.html')
    return HttpResponse(template.render({
        'compras': compras,
    }))

def cadastraCompra(request):
    produtos = Produto.objects.all()
    template = loader.get_template('compras/cadastra.html')
    return HttpResponse(template.render({
        'produtos': produtos,
    }, request))

def salvaCompra(request):
    produto_id = request.POST.get('produto_id')
    preco_compra = request.POST.get('preco_compra')
    quantidade = request.POST.get('quantidade')

    if (int(quantidade) < 1) or (float(preco_compra) <= 0):
        return HttpResponseRedirect("/estoque/compras/novo")
    produto = Produto.objects.get(pk=produto_id)
    compra = Compra(produto=produto, preco_compra=preco_compra, quantidade=quantidade)
    compra.save()
    return HttpResponseRedirect("/estoque/compras")

def excluiCompra(request, id):
    compra = Compra.objects.get(pk=id)
    compra.delete()
    return HttpResponseRedirect("/estoque/compras")